package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
//

/**
 * Created by Divdedov QA on 11/21/2016.
 */
public class Tools2 {

    public static WebDriver webDriver;

    public Tools2()  {

        buildDriver();


    }


    public void buildDriver() {

        //Firefox browser
//
        //FirefoxProfile profile = new FirefoxProfile();
////        profile.setPreference("network.http.phishy-userpass-length", 255);

//        System.setProperty("webdriver.gecko.driver", "C:/geckodriver.exe");
//        webDriver = new FirefoxDriver();
//        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        webDriver.get("http://www.metaporn.com/");
//        webDriver.manage().window().maximize();
//
        //Chrome browser
        //        System.setProperty("webdriver.chrome.driver", "C:/ChromeDriver/chromedriver.exe");
//        webDriver = new ChromeDriver();

        try {
            DesiredCapabilities capability = DesiredCapabilities.chrome();
            webDriver = new RemoteWebDriver(new URL("http://192.168.91.54:4003/wd/hub"),
                    capability);
//            System.setProperty("webdriver.chrome.driver", "C:/ChromeDriver/chromedriver.exe");
//            webDriver = new ChromeDriver();
//
            webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            webDriver.get("https://www.livejasmin.com/en/#chat/");
            webDriver.manage().window().maximize();}
        catch (Exception e) {
            System.out.println(e);
            webDriver.quit();
        }
//
    }

    public void close() {


        webDriver.quit();

    }

    public WebDriver getWebDriver() {
        return webDriver;
    }


    public  WebElement findElementBy(By by) {
        webDriver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(webDriver, 15);
        WebElement element = null;
        element = wait.until(ExpectedConditions.elementToBeClickable(by));
        webDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        return element;
    }


}
