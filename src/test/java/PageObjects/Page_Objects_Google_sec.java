package PageObjects;



import org.junit.Assert;
import org.openqa.selenium.*;


import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import org.json.JSONObject;

import java.rmi.RemoteException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;

import static junit.framework.Assert.assertEquals;

/**
 * Created by Divdedov QA on 2/22/2017.
 */
public class Page_Objects_Google_sec extends Tools {


    public void google_pornhd_check() throws MalformedURLException, InterruptedException {
        webDriver.get("https://www.google.com/transparencyreport/safebrowsing/diagnostic/?hl=en#url=http://www.pornhd.com/");
        System.out.println("Checking PornHD");
        URL url2 = new URL("https://www.google.com/transparencyreport/safebrowsing/diagnostic/?hl=en#url=www.pornhd.com");
        URL url3 = new URL("https://www.google.com/transparencyreport/safebrowsing/diagnostic/?hl=en#url=www.pornrox.com");
        URL url4 = new URL("https://www.google.com/transparencyreport/safebrowsing/diagnostic/?hl=en#url=https://www.pinflix.com/");
        URL url5 = new URL("https://www.google.com/transparencyreport/safebrowsing/diagnostic/?hl=en#url=www.metaporn.com");
        WebElement status_field = findElementBy(By.className("status-not-dangerous"));
        WebElement long_text = findElementBy(By.cssSelector("#status-overview-container > div > p"));

        assertEquals("Not dangerous", status_field.getText());
        assertEquals("Safe Browsing has not recently seen malicious content on www.pornhd.com.", long_text.getText());
        System.out.println("..... OK ");


    }

    public void google_pornrox_check() throws MalformedURLException, InterruptedException {


        webDriver.get("https://www.google.com/transparencyreport/safebrowsing/diagnostic/?hl=en#url=http://www.pornrox.com/");
        webDriver.navigate().refresh();
        System.out.println("Checking Pornrox");

        WebElement status_field = findElementBy(By.className("status-not-dangerous"));
        WebElement long_text = findElementBy(By.cssSelector("#status-overview-container > div > p"));

        Thread.sleep(3000);
        assertEquals("Not dangerous", status_field.getText());
        assertEquals("Safe Browsing has not recently seen malicious content on www.pornrox.com.", long_text.getText());
        System.out.println("..... OK ");
    }


    public void google_pinflix_check() throws MalformedURLException, InterruptedException {


        webDriver.get("https://www.google.com/transparencyreport/safebrowsing/diagnostic/?hl=en#url=https://www.pinflix.com/");
        webDriver.navigate().refresh();

        System.out.println("Checking Pinflix");
        WebElement status_field = findElementBy(By.className("status-not-dangerous"));
        WebElement long_text = findElementBy(By.cssSelector("#status-overview-container > div > p"));

        Thread.sleep(3000);
        assertEquals("Not dangerous", status_field.getText());
        assertEquals("Safe Browsing has not recently seen malicious content on www.pinflix.com.", long_text.getText());
        System.out.println("..... OK ");
    }

    public void google_metaporn_check() throws MalformedURLException, InterruptedException {

        webDriver.get("https://www.google.com/transparencyreport/safebrowsing/diagnostic/?hl=en#url=http://www.metaporn.com/");
        webDriver.navigate().refresh();
        System.out.println("Checking MetaPorn");

        WebElement status_field = findElementBy(By.className("status-not-dangerous"));
        WebElement long_text = findElementBy(By.cssSelector("#status-overview-container > div > p"));

        Thread.sleep(3000);
        assertEquals("Not dangerous", status_field.getText());
        assertEquals("Safe Browsing has not recently seen malicious content on www.metaporn.com.", long_text.getText());
        System.out.println("..... OK ");
    }





}