package Steps;
import PageObjects.Page_objects;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;

import java.text.ParseException;

//
/**
 * Created by Divdedov QA on 9/15/2016.
 */
public class ChannelPage {

    public Page_objects pageObjects = new Page_objects();

    public void destroyDriver() {
        pageObjects.close();
    }

    @Given("^Testing ChannelsPage.")
    public void first_tests() throws InterruptedException, ParseException {

        pageObjects.h1_channels_tab().click();
        Thread.sleep(3000);
        pageObjects.check_color_of_active_h1_tag();
        pageObjects.check_current_URL("http://www.metaporn.com/channels.html");
//        pageObjects.count_video_items_mainpage(96);
//        pageObjects.check_side_bar_h2_titles();
//        pageObjects.count_sidebar_TChannels_items();

//        pageObjects.count_sidebar_TPornstars_items();
//        pageObjects.count_sidebar_TTubes_items(17);
        pageObjects.check_full_meta_description_mainPage_Strings (
                "All porn channels - XXX productions: every studio online! | MetaPorn",
                "Looking for free porn videos from the hottest XXX studios? Find them here at MetaPorn. Amazing clips from your favourite porno channels are waiting for you!",
                "porno,video,porno video,free porno video,porn,porn video,free porn video,porno tube,porn tube,porn tubes, all tubes, free porno,free porn"
        );
        pageObjects.check_canonical_main_pages();
        pageObjects.check_show_buttons();
        pageObjects.check_video_items_h2_second_title("All Channels");
        pageObjects.check_videoContent_min_videosNumber(96);
        pageObjects.check_h1_categories_tag();
//        pageObjects.check_ads_visible();
        pageObjects.count_all_Tags(26);
        pageObjects.check_footer_LEGAL_tabs();
        pageObjects.check_search_bar_featuree();
//
        pageObjects.close();
    }



}
