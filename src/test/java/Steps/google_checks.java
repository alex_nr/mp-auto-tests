package Steps;

import PageObjects.Page_Objects2;
import PageObjects.Page_Objects_Google_sec;
import PageObjects.Tools;
import cucumber.api.java.en.When;
//
/**
 * Created by Divdedov QA on 2/22/2017.
 */
public class google_checks  extends Tools {

    public Page_Objects_Google_sec page_objects_google_SEC = new Page_Objects_Google_sec();

    @When("^Google security checks.")
    public void link_testing() throws Throwable {

        page_objects_google_SEC.google_pornhd_check();
        page_objects_google_SEC.google_pornrox_check();
        page_objects_google_SEC.google_pinflix_check();
        page_objects_google_SEC.google_metaporn_check();
        webDriver.close();
    }



}
