package Steps;
import PageObjects.Page_objects;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;

import java.text.ParseException;


/**
 * Created by Divdedov QA on 9/15/2016.
 */
public class PornstarsPage {


    public Page_objects pageObjects = new Page_objects();;

    public void destroyDriver() {
        pageObjects.close();
    }
//
    @Given("^Testing PornstarsPage.")
    public void first_tests() throws InterruptedException, ParseException {

        pageObjects.h1_pornstars_tab().click();
        Thread.sleep(3000);
        pageObjects.check_color_of_active_h1_tag();
        pageObjects.check_current_URL("http://www.metaporn.com/pornstars.html");
        pageObjects.check_video_items_h2_title("Most Viewed Pornstars");
//        pageObjects.count_video_items_mainpage(60);
       // pageObjects.check_side_bar_h2_titles();
//        pageObjects.count_sidebar_TChannels_items();

//        pageObjects.count_sidebar_TPornstars_items();
//        pageObjects.count_sidebar_TTubes_items(17);
        pageObjects.check_full_meta_description_mainPage_Strings (
                "All porn stars - free pornstar videos: everyone is here! | MetaPorn",
                "Enjoy the sexiest pornstars for free here at MetaPorn. No matter who you desire, you'll find all of their hottest XXX videos right here!",
                "porno,video,porno video,free porno video,porn,porn video,free porn video,porno tube,porn tube,porn tubes, all tubes, free porno,free porn"
        );
        pageObjects.check_canonical_main_pages();

        pageObjects.check_show_buttons();
        pageObjects.check_video_items_h2_second_title("All Pornstars");
        pageObjects.check_videoContent_min_videosNumber(96);
        pageObjects.check_h1_categories_tag();
//        pageObjects.check_ads_visible();
        pageObjects.count_all_Tags(25);
        pageObjects.check_footer_LEGAL_tabs();
        pageObjects.check_search_bar_featuree();

        destroyDriver();
    }


}
//