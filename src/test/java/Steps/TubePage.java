package Steps;
import PageObjects.Page_objects;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;

import java.text.ParseException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by Divdedov QA on 9/15/2016.
 */
public class TubePage {
//
    public Page_objects pageObjects = new Page_objects();

    public void destroyDriver() {
        pageObjects.close();
    }
//
    @Given("^Testing TubePage.")
    public void first_tests() throws InterruptedException, ParseException {

        pageObjects.h1_tubes_tab().click();
        Thread.sleep(3000);
        pageObjects.check_color_of_active_h1_tag();
        pageObjects.check_current_URL("http://www.metaporn.com/tubes.html");
        pageObjects.check_full_meta_description_mainPage_Strings(
                "All porn tubes - XXX sites: every porntube on the Internet! | MetaPorn",
                "Discover XXX video gems from all the biggest porn tubes at MetaPorn. Why should you go to just one site when you can comfortably search all of them right here?",
                "porno,video,porno video,free porno video,porn,porn video,free porn video,porno tube,porn tube,porn tubes, all tubes, free porno,free porn"
        );
        pageObjects.check_canonical_main_pages();
       // pageObjects.check_video_items_h2_title("Most Viewed Tubes");
        pageObjects.check_show_buttons();
        pageObjects.check_video_items_h2_second_title("All Tubes");
        pageObjects.check_videoContent_min_videosNumber(18);
        pageObjects.check_h1_categories_tag();
        pageObjects.check_video_count_matchWith();
//        pageObjects.check_ads_visible();
        pageObjects.count_all_Tags(10);
        pageObjects.check_footer_LEGAL_tabs();
        pageObjects.count_all_tubes_mainPage();
        pageObjects.check_search_bar_featuree();
        pageObjects.close();
    }

}
//