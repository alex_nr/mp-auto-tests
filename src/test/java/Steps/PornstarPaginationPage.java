package Steps;

import PageObjects.Page_objects;
import PageObjects.Tools;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import org.openqa.selenium.WebDriver;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.rmi.RemoteException;

//
/**
 * Created by Divdedov QA on 10/10/2016.
 */
public class PornstarPaginationPage {
    public Page_objects pageObjects = new Page_objects();

//
    public void destroyDriver() {
        pageObjects.close();
    }

    @Given("^Testing PornstarPagination Page.")
    public void first_tests() throws InterruptedException, RemoteException, MalformedURLException, URISyntaxException {
        pageObjects.h1_pornstars_tab().click();
        Thread.sleep(3000);
        pageObjects.Ps_h1_title_check();

        Thread.sleep(3000);

        pageObjects.check_current_URL("http://www.metaporn.com/pornstar/best-zafira/1.html");
        pageObjects.check_full_meta_description_mainPage_Strings (

                "Zafira Porn - Best Videos - Page 1 | MetaPorn",
                "Enjoy Zafira porn movies here on MetaPorn. Watch the hottest free porno videos now! Page 1.",
                "porno,video,porno video,free porno video,porn,porn video,free porn video,porno tube,porn tube,free porno,free porn"
        );



//        pageObjects.check_video_items_h2_title("Best Zafira Porn Videos",0);
//        pageObjects.count_video_items_mainpage(60);
//        pageObjects.check_side_bar_h2_titles();
//        pageObjects.count_sidebar_TChannels_items();
//        pageObjects.count_sidebar_TTags_items();
//        pageObjects.count_sidebar_TPornstars_items();
//        pageObjects.count_sidebar_TTubes_items(17);
        pageObjects.right_pagi_button().click();
        Thread.sleep(3000);
        pageObjects.check_meta_for_PORNSTAR_Pagination_pages_1_2();
        pageObjects.check_full_meta_description_mainPage_Strings (
//
                "Zafira Porn - Best Videos - Page 2 | MetaPorn",
                "Enjoy Zafira porn movies here on MetaPorn. Watch the hottest free porno videos now! Page 2.",
                "porno,video,porno video,free porno video,porn,porn video,free porn video,porno tube,porn tube,free porno,free porn"
        );
//        pageObjects.check_video_items_h2_title("Best Zafira Porn Videos",0);
//        pageObjects.count_video_items_mainpage(60);
//        pageObjects.check_side_bar_h2_titles();
//        pageObjects.count_sidebar_TChannels_items();

//        pageObjects.count_sidebar_TPornstars_items();
//        pageObjects.count_sidebar_TTubes_items(17);
        pageObjects.left_pagi_button().click();
        Thread.sleep(3000);
//        pageObjects.check_video_items_h2_title("Best Zafira Porn Videos (214)",0);
        //pageObjects.video_time_span();
        pageObjects.check_Related();
        //pageObjects.video_under_tag();
        pageObjects.video_pagination_switching();
        //pageObjects.check_ads_visible();
        destroyDriver();

    }
}
