package Steps;
import PageObjects.Page_objects;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import org.openqa.selenium.WebDriver;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.rmi.RemoteException;

//
/**
 * Created by Divdedov QA on 9/20/2016.
 */
public class TagPaginationPage {

    public Page_objects pageObjects = new Page_objects();


    public void destroyDriver() {
        pageObjects.close();
    }

    @Given("^Testing TagPaginationPage.")
    public void first_tests() throws InterruptedException, RemoteException, MalformedURLException, URISyntaxException {
        pageObjects.h1_tags_tab().click();
        Thread.sleep(3000);
        pageObjects.tag_item().click();
        pageObjects.tab_switch();
        pageObjects.check_current_URL("http://www.metaporn.com/tag/best-sex-on-the-floor/1.html");
        pageObjects.check_full_meta_description_mainPage_Strings (

                "Sex On The Floor Porn - Best Videos - Page 1 | MetaPorn",
                "Find the best Sex On The Floor porn movies here on MetaPorn. Stream great free porno clips now! Page 1.",
                "porno,video,porno video,free porno video,porn,porn video,free porn video,porno tube,porn tube,free porno,free porn"
        );

      //  pageObjects.check_video_items_h2_title("Best Sex On The Floor Porn Videos");
//        pageObjects.count_video_items_mainpage(60);
        pageObjects.check_side_bar_h2_titles();
//        pageObjects.count_sidebar_TChannels_items();
//        pageObjects.count_sidebar_TTags_items();
//        pageObjects.count_sidebar_TPornstars_items();
//        pageObjects.count_sidebar_TTubes_items(17);
        pageObjects.right_pagi_button().click();
        pageObjects.check_meta_for_TAG_Pagination_pages_1_2();
        pageObjects.check_full_meta_description_mainPage_Strings (

                "Sex On The Floor Porn - Best Videos - Page 2 | MetaPorn",
                "Find the best Sex On The Floor porn movies here on MetaPorn. Stream great free porno clips now! Page 2.",
                "porno,video,porno video,free porno video,porn,porn video,free porn video,porno tube,porn tube,free porno,free porn"
        );
       // pageObjects.check_video_items_h2_title("Best Sex On The Floor Porn Videos");
//        pageObjects.count_video_items_mainpage(60);
        pageObjects.check_side_bar_h2_titles();
//        pageObjects.count_sidebar_TChannels_items();
//
//        pageObjects.count_sidebar_TPornstars_items();
//        pageObjects.count_sidebar_TTubes_items(17);
        pageObjects.left_pagi_button().click();
        pageObjects.check_full_meta_description_mainPage_Strings (

                "Sex On The Floor Porn - Best Videos - Page 1 | MetaPorn",
                "Find the best Sex On The Floor porn movies here on MetaPorn. Stream great free porno clips now! Page 1.",
                "porno,video,porno video,free porno video,porn,porn video,free porn video,porno tube,porn tube,free porno,free porn"
        );

       // pageObjects.check_video_items_h2_title("Best Sex On The Floor Porn Videos");
        //pageObjects.video_time_span();
        pageObjects.check_Related();
        //pageObjects.video_under_tag();
        pageObjects.video_pagination_switching();
        //pageObjects.check_ads_visible();

        destroyDriver();

    }
}