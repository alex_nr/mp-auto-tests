package Steps;

import PageObjects.Page_objects;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import org.openqa.selenium.WebDriver;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.rmi.RemoteException;
/**
 * Created by Divdedov QA on 10/11/2016.
 */
public class TubePaginagionPage {

    public Page_objects pageObjects = new Page_objects();


    public void destroyDriver() {
        pageObjects.close();
    }

    @Given("^Testing TubePagination Page.")
    public void first_tests() throws InterruptedException, RemoteException, MalformedURLException, URISyntaxException {


        pageObjects.h1_tubes_tab().click();
        Thread.sleep(3000);
        pageObjects.tube_item().click();
        pageObjects.tab_switch();
        pageObjects.check_current_URL("http://www.metaporn.com/tube/best-xvideos/1.html");
        pageObjects.check_full_meta_description_mainPage_Strings(

                "XVideos - Best Videos - Page 1 | MetaPorn",
                "Explore XVideos porn movies here on MetaPorn. Watch the best free XXX videos now! Page 1.",
                "porno,video,porno video,free porno video,porn,porn video,free porn video,porno tube,porn tube,free porno,free porn"
        );


//        pageObjects.count_video_items_mainpage(60);
//        pageObjects.check_side_bar_h2_titles();
//        pageObjects.count_sidebar_TChannels_items();
//        pageObjects.count_sidebar_TTags_items();
//        pageObjects.count_sidebar_TPornstars_items();
//        pageObjects.count_sidebar_TTubes_items(17);
        pageObjects.right_pagi_button().click();
        pageObjects.check_meta_for_Tube_Pagination_pages_1_2();
        pageObjects.check_full_meta_description_mainPage_Strings(

                "XVideos - Best Videos - Page 2 | MetaPorn",
                "Explore XVideos porn movies here on MetaPorn. Watch the best free XXX videos now! Page 2.",
                "porno,video,porno video,free porno video,porn,porn video,free porn video,porno tube,porn tube,free porno,free porn"
        );
//

//        pageObjects.count_video_items_mainpage(60);
        pageObjects.check_side_bar_h2_titles();
//        pageObjects.count_sidebar_TChannels_items();
//        pageObjects.count_sidebar_TTags_items();

//        pageObjects.count_sidebar_TTubes_items(17);
        pageObjects.left_pagi_button().click();

        pageObjects.check_full_meta_description_mainPage_Strings(

                "XVideos - Best Videos - Page 1 | MetaPorn",
                "Explore XVideos porn movies here on MetaPorn. Watch the best free XXX videos now! Page 1.",
                "porno,video,porno video,free porno video,porn,porn video,free porn video,porno tube,porn tube,free porno,free porn"
        );

//

        //pageObjects.video_time_span();
        //pageObjects.check_Related();
        //pageObjects.video_under_tag();
        pageObjects.video_pagination_switching();
        //pageObjects.check_ads_visible();

        destroyDriver();
    }
}
